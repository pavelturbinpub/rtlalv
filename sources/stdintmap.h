// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#pragma once
#include <map>

class StdIntMap : public IntMap
{
    std::map<int, int> mMap;
public:

    StdIntMap()
    {
    }

    virtual ~StdIntMap()
    {
        ClearMap();
    }

    virtual void AddToMap(int key, int value)
    {
        mMap.insert(std::pair<int, int>(key, value));
    }

    virtual bool FindInMap(int key, int &value)
    {
        std::map<int, int>::iterator position = mMap.find(key);
        if (position != mMap.end())
        {
            value = position->second;
            return true;
        }
        return false;
    }

    virtual void ClearMap()
    {
        mMap.clear();
    }

};


