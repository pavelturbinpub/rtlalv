// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#pragma once

class IntMap
{
public:
    IntMap() {}
    virtual ~IntMap() {}

    virtual void AddToMap(int key, int value) = 0;
    virtual bool FindInMap(int key, int &value) = 0;


    virtual void ClearMap() = 0;
};



