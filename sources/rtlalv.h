// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#pragma once

#include <Windows.h>

// definitions copied form ntddk.h
typedef enum _RTL_GENERIC_COMPARE_RESULTS {
    GenericLessThan,
    GenericGreaterThan,
    GenericEqual
} RTL_GENERIC_COMPARE_RESULTS;

typedef
RTL_GENERIC_COMPARE_RESULTS
NTAPI
RTL_AVL_COMPARE_ROUTINE(
    _In_ struct _RTL_AVL_TABLE *Table,
    _In_ PVOID FirstStruct,
    _In_ PVOID SecondStruct
);
typedef RTL_AVL_COMPARE_ROUTINE *PRTL_AVL_COMPARE_ROUTINE;

typedef
PVOID
NTAPI
RTL_AVL_ALLOCATE_ROUTINE(
    _In_ struct _RTL_AVL_TABLE *Table,
    _In_ ULONG ByteSize
);
typedef RTL_AVL_ALLOCATE_ROUTINE *PRTL_AVL_ALLOCATE_ROUTINE;

typedef
VOID
NTAPI
RTL_AVL_FREE_ROUTINE(
    _In_ struct _RTL_AVL_TABLE *Table,
    _In_ __drv_freesMem(Mem) _Post_invalid_ PVOID Buffer
);
typedef RTL_AVL_FREE_ROUTINE *PRTL_AVL_FREE_ROUTINE;

typedef struct _RTL_BALANCED_LINKS {
    struct _RTL_BALANCED_LINKS *Parent;
    struct _RTL_BALANCED_LINKS *LeftChild;
    struct _RTL_BALANCED_LINKS *RightChild;
    CHAR Balance;
    UCHAR Reserved[3];
} RTL_BALANCED_LINKS;
typedef RTL_BALANCED_LINKS *PRTL_BALANCED_LINKS;


typedef struct _RTL_AVL_TABLE {
    RTL_BALANCED_LINKS BalancedRoot;
    PVOID OrderedPointer;
    ULONG WhichOrderedElement;
    ULONG NumberGenericTableElements;
    ULONG DepthOfTree;
    PRTL_BALANCED_LINKS RestartKey;
    ULONG DeleteCount;
    PRTL_AVL_COMPARE_ROUTINE CompareRoutine;
    PRTL_AVL_ALLOCATE_ROUTINE AllocateRoutine;
    PRTL_AVL_FREE_ROUTINE FreeRoutine;
    PVOID TableContext;
} RTL_AVL_TABLE;
typedef RTL_AVL_TABLE *PRTL_AVL_TABLE;

typedef NTSYSAPI
VOID
(NTAPI *RTLINITIALIZEGENERICTABLEAVL) (
    _Out_ PRTL_AVL_TABLE Table,
    _In_ PRTL_AVL_COMPARE_ROUTINE CompareRoutine,
    _In_opt_ PRTL_AVL_ALLOCATE_ROUTINE AllocateRoutine,
    _In_opt_ PRTL_AVL_FREE_ROUTINE FreeRoutine,
    _In_opt_ PVOID TableContext
    );

typedef NTSYSAPI
PVOID
(NTAPI *RTLINSERTELEMENTGENERICTABLEAVL) (
    _In_ PRTL_AVL_TABLE Table,
    _In_reads_bytes_(BufferSize) PVOID Buffer,
    _In_ ULONG BufferSize,
    _Out_opt_ PBOOLEAN NewElement
    );

typedef NTSYSAPI
BOOLEAN
(NTAPI *RTLDELETEGENERICTABLEAVL)(
    _In_ PRTL_AVL_TABLE Table,
    _In_ PVOID Buffer
    );
typedef NTSYSAPI
PVOID
(NTAPI *RTLLOOKUPELEMENTGENERICTABLEAVL) (
    _In_ PRTL_AVL_TABLE Table,
    _In_ PVOID Buffer
    );
typedef NTSYSAPI
PVOID
(NTAPI *RTLENUMERATEGENERICTABLEAVL) (
    _In_ PRTL_AVL_TABLE Table,
    _In_ BOOLEAN Restart
    );

