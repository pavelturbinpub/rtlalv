// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#pragma once
#include "rtlapi.h"

class RtlIntMap : public IntMap
{
    struct MapItem
    {
        int key;
        int value;
    };
    static HANDLE m_Library;
    HANDLE m_Table;

public:

    static bool Load()
    {
        m_Library = LoadRtlAlv();
        if (!m_Library)
            return false;
        return true;
    }

    static void Unload()
    {
        FreeRtlAlv(m_Library);
    }


    RtlIntMap()
    {
        m_Table = InitializeRtlAlvTable(m_Library, RTL_AVL_COMPARE_ROUTINE);
    }

    virtual ~RtlIntMap() 
    {
        ClearMap();
    }

    virtual void AddToMap(int key, int value)
    {
        MapItem item = { key, value };
        InsertItemInRtlAlvTable(m_Library, m_Table, &item, sizeof(item));
    }

    virtual bool FindInMap(int key, int &value)
    {
        MapItem item = { key, 0};
        MapItem *pFound = static_cast<MapItem*>(FindItemInRtlAlvTable(m_Library, m_Table, &item));
        if (!pFound)
            return false;
        value = pFound->value;
        return true;
    }

    virtual void ClearMap()
    {
        ClearRtlAlvTable(m_Library, m_Table);
    }

private:

    static MY_RTL_GENERIC_COMPARE_RESULTS __stdcall RTL_AVL_COMPARE_ROUTINE(
        _In_ PVOID Table,
        _In_ PVOID FirstStruct,
        _In_ PVOID SecondStruct
    )
    {
        const MapItem *firstItem = static_cast<MapItem*>(FirstStruct);
        const MapItem *seconItem = static_cast<MapItem*>(SecondStruct);

        if (firstItem->key == seconItem->key)
            return MyGenericEqual;
        if (firstItem->key < seconItem->key)
            return MyGenericLessThan;
        return MyGenericGreaterThan;

    }
};

