// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#include "rtlalv.h"
#include "rtlapi.h"


typedef struct _RTL_ALV_LIBRARY
{
    HMODULE             hNtdll;
    RTLINITIALIZEGENERICTABLEAVL            fnRtlInitializeGenericTableAvl;
    RTLINSERTELEMENTGENERICTABLEAVL         fnRtlInsertElementGenericTableAvl;
    RTLLOOKUPELEMENTGENERICTABLEAVL         fnRtlLookupElementGenericTableAvl;
    RTLENUMERATEGENERICTABLEAVL             fnRtlEnumerateGenericTableAvl;
    RTLDELETEGENERICTABLEAVL                fnRtlDeleteElementGenericTableAvl;
} RTL_ALV_LIBRARY;


typedef struct _RTL_ALV_WORKING_TABLE
{
    RTL_AVL_TABLE Table;

} RTL_ALV_WORKING_TABLE;


HANDLE LoadRtlAlv()
{
    RTL_ALV_LIBRARY *RtlLibrary;
    RtlLibrary = malloc(sizeof(RTL_ALV_LIBRARY));
    BOOL Status = FALSE;
    do
    {
        RtlLibrary->hNtdll = GetModuleHandle(L"ntdll.dll");
        if (!RtlLibrary->hNtdll)
            break;

        RtlLibrary->fnRtlInitializeGenericTableAvl = (RTLINITIALIZEGENERICTABLEAVL)GetProcAddress(RtlLibrary->hNtdll, "RtlInitializeGenericTableAvl");
        RtlLibrary->fnRtlInsertElementGenericTableAvl = (RTLINSERTELEMENTGENERICTABLEAVL)GetProcAddress(RtlLibrary->hNtdll, "RtlInsertElementGenericTableAvl");
        RtlLibrary->fnRtlLookupElementGenericTableAvl = (RTLLOOKUPELEMENTGENERICTABLEAVL)GetProcAddress(RtlLibrary->hNtdll, "RtlLookupElementGenericTableAvl");
        RtlLibrary->fnRtlEnumerateGenericTableAvl = (RTLENUMERATEGENERICTABLEAVL)GetProcAddress(RtlLibrary->hNtdll, "RtlEnumerateGenericTableAvl");
        RtlLibrary->fnRtlDeleteElementGenericTableAvl = (RTLDELETEGENERICTABLEAVL)GetProcAddress(RtlLibrary->hNtdll, "RtlDeleteElementGenericTableAvl");
        if (!RtlLibrary->fnRtlInitializeGenericTableAvl) // if this loaded, assumed other loaded as well
            break;
        Status = TRUE;

    } while (0);

    if (!Status)
    {
        free(RtlLibrary);
        RtlLibrary = NULL;
    }

    return (HANDLE)RtlLibrary;
}

void   FreeRtlAlv(HANDLE hLibrary)
{
    RTL_ALV_LIBRARY *RtlLibrary = (RTL_ALV_LIBRARY *)hLibrary;
    free(RtlLibrary);
}

static PVOID  NTAPI TableAllocator( _In_ const PRTL_AVL_TABLE pTable, _In_ const ULONG lByteSize) {
    return malloc(lByteSize);
}

static VOID  NTAPI TableFree( _In_ const PRTL_AVL_TABLE pTable, _In_ PVOID pvBuffer) {
    free(pvBuffer);
}

HANDLE InitializeRtlAlvTable(HANDLE hLibrary, MY_RTL_AVL_COMPARE_ROUTINE ItemCompare)
{
    RTL_ALV_LIBRARY *RtlLibrary = (RTL_ALV_LIBRARY *)hLibrary;
    RTL_ALV_WORKING_TABLE *RtlWorkingTable = malloc(sizeof(RTL_ALV_WORKING_TABLE));

    RtlLibrary ->fnRtlInitializeGenericTableAvl(&RtlWorkingTable->Table, (PRTL_AVL_COMPARE_ROUTINE) ItemCompare, TableAllocator, TableFree, NULL);

    return (HANDLE)RtlWorkingTable;
}

void   InsertItemInRtlAlvTable(HANDLE hLibrary, HANDLE hTable, PVOID Buffer, ULONG Size)
{
    RTL_ALV_LIBRARY *RtlLibrary = (RTL_ALV_LIBRARY *)hLibrary;
    RTL_ALV_WORKING_TABLE *RtlWorkingTable = (RTL_ALV_WORKING_TABLE *)hTable;

    BOOLEAN NewElement = FALSE;
    RtlLibrary->fnRtlInsertElementGenericTableAvl(&RtlWorkingTable->Table, Buffer, Size, &NewElement);
}

PVOID  FindItemInRtlAlvTable(HANDLE hLibrary, HANDLE hTable, PVOID ToFind)
{
    RTL_ALV_LIBRARY *RtlLibrary = (RTL_ALV_LIBRARY *)hLibrary;
    RTL_ALV_WORKING_TABLE *RtlWorkingTable = (RTL_ALV_WORKING_TABLE *)hTable;
    return RtlLibrary->fnRtlLookupElementGenericTableAvl(&RtlWorkingTable->Table, ToFind);
}

void   ClearRtlAlvTable(HANDLE hLibrary, HANDLE hTable)
{
    RTL_ALV_LIBRARY *RtlLibrary = (RTL_ALV_LIBRARY *)hLibrary;
    RTL_ALV_WORKING_TABLE *RtlWorkingTable = (RTL_ALV_WORKING_TABLE *)hTable;

    for (PVOID todelete = (PVOID)RtlLibrary->fnRtlEnumerateGenericTableAvl(&RtlWorkingTable->Table, TRUE);
        todelete != NULL;
        todelete = (PVOID)RtlLibrary->fnRtlEnumerateGenericTableAvl(&RtlWorkingTable->Table, FALSE)
        )
    {
        RtlLibrary->fnRtlDeleteElementGenericTableAvl(&RtlWorkingTable->Table, todelete);
    }
}



