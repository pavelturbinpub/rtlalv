// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#pragma once

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum _MY_RTL_GENERIC_COMPARE_RESULTS { // must match to RTL_GENERIC_COMPARE_RESULTS 
    MyGenericLessThan,
    MyGenericGreaterThan,
    MyGenericEqual
} MY_RTL_GENERIC_COMPARE_RESULTS;


typedef MY_RTL_GENERIC_COMPARE_RESULTS
(__stdcall *MY_RTL_AVL_COMPARE_ROUTINE)(
    _In_ PVOID Table,
    _In_ PVOID FirstStruct,
    _In_ PVOID SecondStruct
);

HANDLE LoadRtlAlv();
void   FreeRtlAlv(HANDLE hLibrary);

HANDLE InitializeRtlAlvTable(HANDLE hLibrary, MY_RTL_AVL_COMPARE_ROUTINE ItemCompare);
void   InsertItemInRtlAlvTable(HANDLE hLibrary, HANDLE hTable, PVOID Buffer, ULONG Size);
PVOID  FindItemInRtlAlvTable(HANDLE hLibrary, HANDLE hTable, PVOID ToFind);
void   ClearRtlAlvTable(HANDLE hLibrary, HANDLE hTable);


#ifdef __cplusplus
}
#endif /* __cplusplus */


