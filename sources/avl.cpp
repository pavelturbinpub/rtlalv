// Comparison between std::map and Rtl...GenericTableAvl 
// Copyright (C) Pavel Turbin, 2017.

#include "stdafx.h"
#include "intmap.h"
#include "rtlintmap.h"
#include "stdintmap.h"

HANDLE RtlIntMap::m_Library = NULL;


bool validateMap(IntMap &toValidate)
{
    toValidate.AddToMap(49, 19);
    int value = 0;
    if (!toValidate.FindInMap(49, value) || value != 19)
        return false;
    if( toValidate.FindInMap(19, value) )
        return false;
    toValidate.ClearMap();
    if (toValidate.FindInMap(49, value))
        return false;

    toValidate.AddToMap(19, 49);
    toValidate.AddToMap(19, 49);
    toValidate.AddToMap(19, 49);
    toValidate.AddToMap(19, 49);

    if (!toValidate.FindInMap(19, value) || value != 49)
        return false;
    toValidate.AddToMap(10, 20);
    if (!toValidate.FindInMap(19, value) || value != 49)
        return false;

    return true;
}

bool validateMap()
{
    RtlIntMap  intMapWithRtl;
    if (!validateMap(intMapWithRtl))
        return false;

    StdIntMap intMapWithStd;
    if (!validateMap(intMapWithStd))
        return false;

    return true;
}

int getRandom()
{
    return rand();
}

int stressMapInsertFind(IntMap &toValidate)
{
    int found = 0;
    for (int iter = 0; iter < 100; iter++) 
    {
        toValidate.ClearMap();
        for (int i = 0; i < 100; i++)
        {
            toValidate.AddToMap(i, i);
        }

        for (int i = 0; i < 50000; i++)
        {
            toValidate.AddToMap(rand(), rand());
        }

        for (int i = 0; i < 50000; i++)
        {
            int value;
            if (toValidate.FindInMap(i, value))
                found++;
        }
    }
    return found;
}

int stressMapFind(IntMap &toValidate)
{
    int found = 0;
    toValidate.ClearMap();
    for (int i = 0; i < 100; i++)
    {
        toValidate.AddToMap(i, i);
    }

    for (int i = 0; i < 50000; i++)
    {
        toValidate.AddToMap(rand(), rand());
    }

    for (int iter = 0; iter < 1000; iter++)
    {
        for (int i = 0; i < 50000; i++)
        {
            int value;
            if (toValidate.FindInMap(i, value))
                found++;
        }
    }
    return found;
}


int stressMap()
{
    RtlIntMap  intMapWithRtl;
    StdIntMap intMapWithStd;
    DWORD Start = GetTickCount();

    if (stressMapInsertFind(intMapWithRtl) < 49)
        return 0;

    printf("INSERT-FIND rtl map took %d ms\n", GetTickCount() - Start);

    Start = GetTickCount();
    if (stressMapInsertFind(intMapWithStd) < 49)
        return 0;

    printf("INSERT-FIND std map took %d ms\n", GetTickCount() - Start);

    Start = GetTickCount();
    if (stressMapFind(intMapWithRtl) < 49)
        return 0;

    printf("FIND rtl map took %d ms\n", GetTickCount() - Start);

    Start = GetTickCount();
    if (stressMapFind(intMapWithStd) < 49)
        return 0;

    printf("FIND std map took %d ms\n", GetTickCount() - Start);

    return 1;
}

int main()
{
    if (!RtlIntMap::Load())
    {
        printf("failed to init RTL library\n");
        return 1;
    }

    if (validateMap())
    {
        printf("maps are consistent\n");
    }
    else
    {
        printf("maps failed to validate, aborting\n");
        return 1;
    }

    if (!stressMap())
    {
        printf("failed to stress map, aborting\n");
        return 1;
    }


    return 0;
}

